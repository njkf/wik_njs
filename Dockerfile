FROM node:12-alpine

RUN apk update && \
apk add \
nano \
vim && \
mkdir /app

WORKDIR /app

COPY . /app

RUN npm install

CMD npm start
